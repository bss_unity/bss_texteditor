Unity 내에서 간단한 텍스트 편집을 지원하는 툴입니다.

## FEATURES 
- Editing
- Save (Ctrl+S)
- Revert (Ctrl+R)
- Undo Save
- Json Format Validate

## COMPATIBILITY
- Requires Unity 2018.3 or above
- Requires .NET 4.x

## HOW TO
1. Go to 'Tools/BSS/Editor/Text Editor'
2. Select Text Asset.
3. Enjoy it.

### CREATE BY
- Sanghun Lee (Unity Client Developer)
- Email: tkdgns1284@gmail.com
- Gitlab: https://gitlab.com/tkdgns1284